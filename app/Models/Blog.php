<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'status',
        'image',
        'slug',
        'description',
        'seo_details',
        'publish_date',
        'tags',
        'user_id'
    ];
    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_blogs');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
