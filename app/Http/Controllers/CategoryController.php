<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use DataTables;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = Category::select('*');

            return Datatables::of($data)

                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="space-child"><a class="btn btn-primary btn-sm" href="'.route('admin.categories.edit',['id'=>$row->id]).'">
                           <i class="fa fa-pencil"></i> Edit
                         </a>
                         <a class="btn btn-danger btn-sm delete-btn" href="'.route('admin.categories.delete',['id'=>$row->id]).'">
                         <i class="fa fa-trash"></i> Delete
                       </a></div>
                         ';
                            return $btn;
                    })
                    

                    ->rawColumns(['action'])

                    ->make(true);

        }
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:191'

            ]);
        $input=$request->all();
           $data=Category::create($input);
        Session::flash('success', 'Data modified!'); 

        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $data=Category::findOrFail($id);
        return view('admin.categories.edit',compact('data'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:191',
            ]);
        $input=$request->all();
        $data=Category::find($input['id'])->update($input);

        Session::flash('success', 'Data modified!'); 

        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $data=Category::find($id)->delete();
        Session::flash('delete', 'Data deleted!'); 
        return redirect()->route('admin.categories.index');
    }
}
