<?php

namespace App\Http\Controllers\Editor;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use DataTables;
use Session;
use Str;
use Carbon\Carbon;
use Storage;
use Auth;
class BlogController extends Controller
{
    public function index(Request $request)
    {
        $user=Auth::user();
        if ($request->ajax()) {

            $data = Blog::select('*')->where('user_id',$user->id);

            return Datatables::of($data)

                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                        if($row->status==1){
                            $btn = ' <div class="px-25 py-10 w-100"><span class="badge badge-success">Published</span></div>';
                        }else{
                            $btn = ' <div class="px-25 py-10 w-100"><span class="badge badge-danger">Unpublished</span></div>';
                        }
                    
                         return $btn;
                 })
                 ->addColumn('image', function($row){
                    $storagePath  = '/storage';

                    $image='<img src="'.$storagePath.$row->image.'">';
                     return $image;
             })
                 
                    ->addColumn('action', function($row){
                           $btn = '<div class="space-child"><a class="btn btn-primary btn-sm" href="'.route('editor.blogs.edit',['id'=>$row->id]).'">
                           <i class="fa fa-pencil"></i> Edit
                         </a>
                         <a class="btn btn-danger btn-sm delete-btn" href="'.route('editor.blogs.delete',['id'=>$row->id]).'">
                         <i class="fa fa-trash"></i> Delete
                       </a></div>
                         ';
                            return $btn;
                    })
                    

                    ->rawColumns(['action','status','image'])

                    ->make(true);

        }

        return view('editor.blogs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();

        return view('editor.blogs.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=Auth::user();
        $input=$request->all();
        $input['slug']=Str::slug($request->title);
        $input['publish_date']=Carbon::now()->toDateTimeString();
        $input['slug']=Str::slug($request->title);
        $input['user_id']=$user->id;
        $request['slug']=$input['slug'];
        $this->validate($request,[
            'slug' => "required|unique:blogs,slug|max:191",
        ]);
        if($request->hasFile('image')) {

            //get filename with extension
            $filenamewithextension = $request->file('image')->getClientOriginalName();
    
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
    
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
    
            //filename to store
            $filenametostore = $filename.'_'.uniqid().'.'.$extension;
    
            //Upload File to external server
            Storage::disk('blogs')->put($filenametostore, fopen($request->file('image'), 'r+'));
    
            //Store $filenametostore in the database
            // $input['image']=$filenametostore;
            $input['image']='/blogs/'.$filenametostore;

        }
       
        $data=Blog::create($input);
        $category = Category::find($input['categories']);
        $data->categories()->attach($category);
        
        Session::flash('success', 'Data modified!'); 
        
        return redirect()->route('editor.blogs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Blog::with('categories')->find($id);
        $blog_cat=$data->categories()->get()->pluck('id')->toArray();
        $categories=Category::all();

        return view('editor.blogs.edit',compact('categories','data','blog_cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
        $user=Auth::user();
        $input=$request->all();
        $input['slug']=Str::slug($request->title);
        $input['publish_date']=Carbon::now()->toDateTimeString();
        $input['slug']=Str::slug($request->title);
        $input['user_id']=$user->id;

        $request['slug']=$input['slug'];
        $data=Blog::find($input['id']);
        $this->validate($request,[
            'slug' => 'required|max:191|unique:blogs,slug,'.$data->id,
        ]);
        if($request->hasFile('image')) {

            //get filename with extension
            $filenamewithextension = $request->file('image')->getClientOriginalName();
    
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
    
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
    
            //filename to store
            $filenametostore = $filename.'_'.uniqid().'.'.$extension;
    
            //Upload File to external server
            Storage::disk('blogs')->put($filenametostore, fopen($request->file('image'), 'r+'));
    
            //Store $filenametostore in the database
            // $input['image']=$filenametostore;
            $input['image']='/blogs/'.$filenametostore;

        }

       
        $data->update($input);

        $category = Category::find($input['categories']);
        $data->categories()->detach();
        $data->categories()->attach($category);
        
        
        Session::flash('success', 'Data modified!'); 
        return redirect()->route('editor.blogs.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $data=Blog::find($id)->delete();
        Session::flash('delete', 'Data deleted!'); 
        return redirect()->route('editor.blogs.index');
    }
}
