<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Category;
use App\Models\User;



class SearchController extends Controller
{
   public function search(Request $request){

   
    $data=new Blog;
    if(!empty($request->keyword)||$request->keyword!=''){
       $data=$data->where('title', 'like', "%{$request->keyword}%");
    }
    if(!empty($request->status)||$request->status!=''){
        $data=$data->where('status',$request->status);
    }   
    if(!empty($request->categories)||$request->categories!=''){
       
        $categories=$request->categories;
        $data=$data->whereHas('categories', function($q) use($categories) {
            $q->whereIn('category_id', $categories);
        });
    }  
    if(!empty($request->users)||$request->users!=''){
       
        $users=$request->users;
        $data=$data->whereHas('user', function($q) use($users) {
            $q->whereIn('user_id', $users);
        });
    }  
    $data=$data->get();
    $categories=Category::all();
    $users=User::all();


    return view('admin.search.index', [
        'data' => $data,
        'categories' => $categories,
        'users' => $users,

    ]);

   }
}
