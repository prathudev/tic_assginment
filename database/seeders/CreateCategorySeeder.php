<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
class CreateCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Category = [
            [
                'name' => 'Travel',
            ],
            [
                'name' => 'Music',
            ],

            [
                'name' => 'Fitness',
            ],
            [
                'name' => 'Fashion',
            ],
        ];
        Category::insert($Category);
    }
}
