<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();
Route::group(['prefix' => 'admin',  'middleware' => ['auth','is_admin']], function(){
    // Route::resource('users', 'UsersController');
    Route::get('categories', [App\Http\Controllers\CategoryController::class, 'index'])->name('admin.categories.index');
    Route::get('categories/create', [App\Http\Controllers\CategoryController::class, 'create'])->name('admin.categories.create');
    Route::post('categories/store', [App\Http\Controllers\CategoryController::class, 'store'])->name('admin.categories.store');
    Route::get('categories/edit/{id}', [App\Http\Controllers\CategoryController::class, 'edit'])->name('admin.categories.edit');
    Route::put('categories/update', [App\Http\Controllers\CategoryController::class, 'update'])->name('admin.categories.update');
    Route::get('categories/delete/{id}', [App\Http\Controllers\CategoryController::class, 'delete'])->name('admin.categories.delete');


    Route::get('blogs', [App\Http\Controllers\BlogController::class, 'index'])->name('admin.blogs.index');
    Route::get('blogs/create', [App\Http\Controllers\BlogController::class, 'create'])->name('admin.blogs.create');
    Route::post('blogs/store', [App\Http\Controllers\BlogController::class, 'store'])->name('admin.blogs.store');
    Route::get('blogs/edit/{id}', [App\Http\Controllers\BlogController::class, 'edit'])->name('admin.blogs.edit');
    Route::put('blogs/update', [App\Http\Controllers\BlogController::class, 'update'])->name('admin.blogs.update');
    Route::get('blogs/delete/{id}', [App\Http\Controllers\BlogController::class, 'delete'])->name('admin.blogs.delete');

    Route::get('search', [App\Http\Controllers\SearchController::class, 'search'])->name('admin.search');

});

Route::group(['prefix' => 'editor',  'middleware' => ['auth']], function(){
    // Route::resource('users', 'UsersController');


    Route::get('blogs', [App\Http\Controllers\Editor\BlogController::class, 'index'])->name('editor.blogs.index');
    Route::get('blogs/create', [App\Http\Controllers\Editor\BlogController::class, 'create'])->name('editor.blogs.create');
    Route::post('blogs/store', [App\Http\Controllers\Editor\BlogController::class, 'store'])->name('editor.blogs.store');
    Route::get('blogs/edit/{id}', [App\Http\Controllers\Editor\BlogController::class, 'edit'])->name('editor.blogs.edit');
    Route::put('blogs/update', [App\Http\Controllers\Editor\BlogController::class, 'update'])->name('editor.blogs.update');
    Route::get('blogs/delete/{id}', [App\Http\Controllers\Editor\BlogController::class, 'delete'])->name('editor.blogs.delete');

});

// Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
// Route::get('home', [HomeController::class, 'index'])->name('home');
