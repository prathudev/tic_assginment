@extends('admin.layout')
@section('content')
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Create Category</h3>
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-12">
					<div class="box">
						<!-- <div class="box-header with-border">
							<h4 class="box-title">Change Password</h4>
						</div> -->
						<div class="box-body">
					
							<form action="{{route('admin.categories.store')}}" method="post" enctype="multipart/form-data">
                                
                                @csrf 
                                <div class="form-group row">
									<label class="col-form-label col-md-2">Category name</label>
									<div class="col-md-10">
										<input id="name" class="form-control" type="text" name="name">
									</div>
								</div>
                              
								
								<div class="form-group row">
								
									<div class="col-md-10">
										<button type="submit" class="btn bg-primary">Submit</button>
										
									</div>
								</div>
							</form>
						</div>
						
					</div>
				</div>
		</div>
@endsection