@extends('admin.layout')
@section('content')
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Edit Category</h3>
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-12">
					<div class="box">
						<!-- <div class="box-header with-border">
							<h4 class="box-title">Change Password</h4>
						</div> -->
						<div class="box-body">

							<form action="{{route('admin.categories.update')}}" method="post" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf 
                               <input type="hidden" name="id" value="{{$data->id}}">
							   <div class="form-group row">
									<label class="col-form-label col-md-2">Name</label>
									<div class="col-md-10">
										<input id="name" class="form-control" type="text" name="name" value="{{$data->name}}">
									</div>
								</div>
															
								<div class="form-group row">
								
									<div class="col-md-10">
										<button type="submit" class="btn bg-primary">Submit</button>
										
									</div>
								</div>
							</form>
						</div>
						
					</div>
				</div>
		</div>
@endsection