
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="/images/logo-icon.png">

    <title> Admin - Dashboard</title>
    
	<link rel="stylesheet" href="/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	
	
	<!-- Bootstrap select -->
	<link rel="stylesheet" href="/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css">

	<link rel="stylesheet" href="/assets/vendor_components/select2/dist/css/select2.min.css">

	 <!-- toast CSS -->
	 <link href="/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css" rel="stylesheet">

	 <link href="/assets/vendor_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

	<!-- theme style -->
	<link rel="stylesheet" href="/css/style.css">
	
	<!-- Admin skins -->
	<link rel="stylesheet" href="/css/skin_color.css">
	<link rel="stylesheet" href="/assets/country-picker-flags/css/countrySelect.min.css">
	<link rel="stylesheet" href="/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
	<style>
	.art-bg{
		background: linear-gradient(to right, #39bdd2 0%, #f2ec23 100%) !important;
	}
	.min-width100{
		min-width: 150px;
	}
	.space-child>a{
		margin-left: 10px;
	}
	.language.active{
		color:#f88d04;
	}
	.light-logo img{
		height:40px;
	}
	</style>
	@yield('css')
  </head>

<body class="hold-transition light-skin sidebar-mini theme-school onlywave">
	
<div class="wrapper" id="app">
	
  <div class="art-bg">
	  <img src="/images/art1.svg" alt="" class="art-img light-img">
	  <img src="/images/art2.svg" alt="" class="art-img dark-img">
	  <img src="/images/art-bg.svg" alt="" class="wave-img light-img">
	  <img src="/images/art-bg2.svg" alt="" class="wave-img dark-img">
  </div>

  <header class="main-header">
    <!-- Logo -->
    <a href="index.html" class="logo">
      <!-- mini logo -->
	  <div class="logo-mini">
		  <span class="light-logo"><img src="/images/logo-light.png" alt="logo"></span>
		  <span class="dark-logo"><img src="/images/logo-dark.png" alt="logo"></span>
	  </div>
      <!-- logo-->
      <div class="logo-lg">
		  <span class="light-logo"><img src="/images/logo-light-text.png" alt="logo"></span>
	  	  <span class="dark-logo"><img src="/images/logo-dark-text.png" alt="logo"></span>
	  </div>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
		
	  <div class="app-menu">
		<ul class="header-megamenu nav">
			<li class="btn-group nav-item">
				<a href="#" class="nav-link rounded" data-toggle="push-menu" role="button">
					<i class="nav-link-icon mdi mdi-menu text-white"></i>
			    </a>
			</li>
			<li class="btn-group nav-item">
				<a href="#" data-provide="fullscreen" class="nav-link rounded" title="Full Screen">
					<i class="nav-link-icon mdi mdi-crop-free text-white"></i>
			    </a>
			</li>
	
			
		</ul> 
	  </div>
		
      <div class="navbar-custom-menu r-side">

        <ul class="nav navbar-nav">
		  <!-- full Screen -->
	    		
	

		  
			

		  
		  <!-- User Account-->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="User">
              <img src="/images/avatar/7.jpg" class="user-image rounded-circle" alt="User Image">
            </a>
            <ul class="dropdown-menu animated flipInX">
              <!-- User image -->
              <li class="user-header bg-img" style="background-image: url(../images/user-info.jpg)" data-overlay="3">
				  <div class="flexbox align-self-center">					  
				  	<img src="/images/avatar/7.jpg" class="float-left rounded-circle" alt="User Image">					  
					<h4 class="user-name align-self-center">
					  <span>{{auth()->user()->name }} </span>
					  <small>{{auth()->user()->email }}</small>
					</h4>
				  </div>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                  {{--
				    <a class="dropdown-item" href="{{route('admin.profile')}}"><i class="ion ion-person"></i> My Profile</a>
                    --}}
					<a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="ion-log-out"></i> Logout</a>
					<div class="dropdown-divider"></div>
					<!-- <div class="p-10"><a href="javascript:void(0)" class="btn btn-sm btn-rounded btn-success">View Profile</a></div> -->
              </li>
            </ul>
          </li>	
			
	
    
			
        </ul>
      </div>
    </nav>
  </header>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full clearfix position-relative">	
  
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar-->
			<section class="sidebar">
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">

              

                <li class="{{ Request::routeIs('admin.categories.index') ? 'active' : '' }}">
				  <a href="{{route('admin.categories.index')}}">
					<i class="fa fa-list"></i>
					<span>{{ __('All categories') }}</span>
				  </a>
				</li> 
				<li class="{{ Request::routeIs('admin.blogs.index') ? 'active' : '' }}">
				  <a href="{{route('admin.blogs.index')}}">
					<i class="fa fa-list"></i>
					<span>{{ __('All Blogs') }}</span>
				  </a>
				</li> 
				<li class="{{ Request::routeIs('admin.search') ? 'active' : '' }}">
				  <a href="{{route('admin.search')}}">
					<i class="fa fa-list"></i>
					<span>{{ __('Search') }}</span>
				  </a>
				</li> 
            {{--
                <li class="{{ Request::routeIs('admin.changePassowrd') ? 'active' : '' }}">
				  <a href="{{route('admin.changePassowrd')}}">
					<i class="fa fa-key"></i>
					<span>{{ __('Change password') }}</span>
				  </a>
				</li> 
            --}}
				<li>
				  <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
					<i class="ti-power-off"></i>
					<span>{{ __('Logout') }}</span>
				  </a>
				  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                  </form>
				</li> 

			  </ul>
			</section>
		</aside>
		<!-- Main content -->
		<section class="content">

            @yield('content')
	

			
						
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  <li class="nav-item">
			<a class="nav-link" href="javascript:void(0)">FAQ</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Purchase Now</a>
		  </li>
		</ul>
    </div> -->
	  &copy; {{date('Y')}} <a href="#">Assignment</a>. All Rights Reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar">
	  
	<div class="rpanel-title"><span class="pull-right btn btn-circle btn-danger"><i class="ion ion-close text-white" data-toggle="control-sidebar"></i></span> </div>  <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab" title="Notifications"><i class="ti-comment-alt"></i></a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab" title="Comments"><i class="ti-tag"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
		  <div class="lookup lookup-sm lookup-right d-none d-lg-block mb-20">
			<input type="text" name="s" placeholder="Search" class="w-p100">
		  </div>
          <div class="media-list media-list-hover">
			<a class="media media-single" href="#">
			  <h4 class="w-50 text-gray font-weight-500">10:10</h4>
			  <div class="media-body pl-15 bl-5 rounded border-primary">
				<p>Morbi quis ex eu arcu auctor sagittis.</p>
				<span class="text-fade">by Johne</span>
			  </div>
			</a>

			<a class="media media-single" href="#">
			  <h4 class="w-50 text-gray font-weight-500">08:40</h4>
			  <div class="media-body pl-15 bl-5 rounded border-success">
				<p>Proin iaculis eros non odio ornare efficitur.</p>
				<span class="text-fade">by Amla</span>
			  </div>
			</a>

			<a class="media media-single" href="#">
			  <h4 class="w-50 text-gray font-weight-500">07:10</h4>
			  <div class="media-body pl-15 bl-5 rounded border-info">
				<p>In mattis mi ut posuere consectetur.</p>
				<span class="text-fade">by Josef</span>
			  </div>
			</a>

			<a class="media media-single" href="#">
			  <h4 class="w-50 text-gray font-weight-500">01:15</h4>
			  <div class="media-body pl-15 bl-5 rounded border-danger">
				<p>Morbi quis ex eu arcu auctor sagittis.</p>
				<span class="text-fade">by Rima</span>
			  </div>
			</a>

			<a class="media media-single" href="#">
			  <h4 class="w-50 text-gray font-weight-500">23:12</h4>
			  <div class="media-body pl-15 bl-5 rounded border-warning">
				<p>Morbi quis ex eu arcu auctor sagittis.</p>
				<span class="text-fade">by Alaxa</span>
			  </div>
			</a>

		  </div>
      </div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
		  <div class="media-list media-list-hover media-list-divided">
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/1.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
				<p>Cras tempor diam nec metus...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (22)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/2.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
				<p>Praesent tristique diam...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (23)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/1.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
				<p>Cras tempor diam nec...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (22)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/2.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
				<p>Praesent tristique diam...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (23)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/1.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
				<p>Cras tempor diam nec metus...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (22)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/2.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
				<p>Praesent tristique diam...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (23)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/1.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
				<p>Cras tempor diam nec...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (22)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
			<div class="media">
			  <img class="avatar avatar-lg" src="/images/avatar/2.jpg" alt="...">

			  <div class="media-body">
				<p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
				<p>Praesent tristique diam...</p>
				<div class="media-block-actions">
				  <nav class="nav nav-dot-separated no-gutters">
					<div class="nav-item">
					  <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i> (17)</a>
					</div>
					<div class="nav-item">
					  <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i> (23)</a>
					</div>
				  </nav>

				  <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
					<a class="nav-link text-success" href="#" data-toggle="tooltip" title="" data-original-title="Approve"><i class="ion-checkmark"></i></a>
					<a class="nav-link text-danger" href="#" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion-close"></i></a>
				  </nav>
				</div>
			  </div>
			</div>
		  </div>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	
	<!-- jQuery 3 -->
	<script src="/assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- fullscreen -->
	<script src="/assets/vendor_components/screenfull/screenfull.js"></script>
	
	<!-- popper -->
	<script src="/assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Bootstrap Select -->
	<script src="/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
	
	<!-- Bootstrap tagsinput -->
	<script src="/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>
	
	<!-- Bootstrap touchspin -->
	<script src="/assets/vendor_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
	
	<!-- Select2 -->
	<script src="/assets/vendor_components/select2/dist/js/select2.full.js"></script>
	
	<!-- InputMask -->
	<script src="/assets/vendor_plugins/input-mask/jquery.inputmask.js"></script>
	<script src="/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js"></script>

	<!-- date-range-picker -->
	<script src="/assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>

	<!-- SlimScroll -->
	<script src="/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	
	<!-- FastClick -->
	<script src="/assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- Sweet-Alert  -->
	<script src="/assets/vendor_components/sweetalert/sweetalert.min.js"></script>

	<script src="/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>

	<!-- Assignment Admin App -->
	<script src="/js/template.js"></script>
	
	<!-- Assignment Admin for demo purposes -->
	<script src="/js/demo.js"></script>
	
	<!-- toast -->
	<script src="/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js"></script>
    <script src="/js/pages/toastr.js"></script>
    <script src="/js/pages/notification.js"></script>
	<script src="/assets/country-picker-flags/js/countrySelect.min.js"></script>
	<link rel="stylesheet" href="/assets/vendor_components/select2/dist/css/select2.min.css">
	<script src="/assets/vendor_components/ckeditor/ckeditor.js"></script>
	<script>
	$("#country_selector").countrySelect();
	$('.select2').select2();

	</script>
	@if(Session::has('success'))
	<script>
	$.toast({
		heading: 'Success',
		text: '{{ \Session::get("success") }}',
		icon: 'success',
		loader: true,        // Change it to false to disable loader
		loaderBg: '#9EC600' , // To change the background
		position: 'top-right',
	})
	</script>
	@endif

	@if(Session::has('delete'))
	<script>
	$.toast({
		heading: 'Warning',
		text: '{{ \Session::get("delete") }}',
		icon: 'warning',
		loader: true,        // Change it to false to disable loader
		loaderBg: '#ff0837' , // To change the background
		position: 'top-right',
	})
	</script>
	@endif

	@foreach ($errors->all() as $error)

	<script>
		$.toast({
			heading: 'Error',
			text: '{{ $error }}',
			icon: 'error',
			loader: true,        // Change it to false to disable loader
			loaderBg: '#9EC600' , // To change the background
			position: 'top-right',
		})
	</script>

	@endforeach 

	<script>
		// $('#sa-warning').click(function(){
		$(document).on("click", ".delete-btn", function(e) {
			
			self=this;
			swal({   
				title: "Are you sure?",   
				text: "You will not be able to recover this imaginary file!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes, delete it!",   
				closeOnConfirm: false 
			}, function(){   
				// swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
				// console.log($(self).attr('href'));
				window.location.href = $(self).attr('href');
			});
			e.preventDefault();
		});
	</script>
	@yield('js')
</body>
</html>
