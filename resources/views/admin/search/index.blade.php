@extends('admin.layout')
@section('css')
	<!-- Data Table-->
	<link rel="stylesheet" type="text/css" href="/assets/vendor_components/datatable/datatables.min.css"/>
	
@endsection
@section('content')
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Blogs</h3>
                      
					</div>
                 
				</div>
          
               
		</div>
        <div class="row">
				<div class="col-12">
                    <div class="card">
                            <div class="box-header with-border">
                                    <h4 class="box-title">Search</h4>
                            </div> 
                            <div class="box-body">
                                <form  method="get" >
                                <div class="row">
                                    <div class="col">
                                        <label for="keyword">Keyword</label>
                                        <input type="text" class="form-control" placeholder="Keyword" aria-label="keyword" id="keyword" name="keyword">
                                    </div>
                                    <div class="col">
                                        <label for="categories">Category</label>
                                       <select name="categories[]" id="categories" class="select2 form-control" multiple>
                                          <option value="">Select option</option>
                                           @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                    <div class="col">
                                       <label for="users">Author</label>
                                       <select name="users[]" id="users" class="select2 form-control" multiple>
                                          <option value="">Select option</option>
                                           @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                    <div class="col">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select option</option>
											<option value="1">Publish</option>
											<option value="2">Unpublish</option>
										</select>
                                    </div>
                                    <div class="col">
                                        <br>
                                        <button type="submit" class="btn bg-primary">Submit</button>
                                    </div>
                                    @if (request()->hasAny(['keyword', 'categories', 'status','users']))
                                    <div class="col">
                                        <br>
                                        <a href="{{route('admin.search')}}" class="btn bg-primary">Clear</a>
                                    </div>
                                    @endif
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
        </div>
		<div class="row">
				<div class="col-12">
					<div class="box"> 
						<div class="box-header with-border">
								<h3 class="box-title">All Blogs</h3>
						</div>
						<div class="table-responsive">						
							<table class="table table-bordered table-striped data-table">
								<thead>

									<tr>
										
										<th>ID</th>
										<th>Title</th>
										<th>Categories</th>
										<th>Image</th>
										<th>Author</th>
										<th>Status</th>
										<th>Created at</th>
									

									</tr>

								</thead>

								<tbody>
                                    @foreach ($data as $blog)
                                    <tr>
                                        <td>{{$blog->id}}</td>
                                        <td>{{$blog->title}}</td>
                                        <td>
                                            @foreach($blog->categories as $cat)
                                                <div class="px-25 py-10 w-100"><span class="badge badge-primary">{{$cat->name}}</span></div>  
                                            @endforeach
                                        </td>
                                        <td><img src="{{'/storage'.$blog->image}}" alt="" srcset="" height="50px;"></td>
                                        <td>{{$blog->user->name}}</td>
                                        <td>
                                        @if($blog->status==1)
                                        <div class="px-25 py-10 w-100"><span class="badge badge-success">Published</span></div>
                                        @else
                                        <div class="px-25 py-10 w-100"><span class="badge badge-danger">Unpublished</span></div>
                                        @endif
                                            
                                        </td>
                                        <td>{{$blog->created_at}}</td>
                                    </tr>
                                    @endforeach
								</tbody>
                               
							</table>
                          
						</div>
                      
					</div>
                    
				</div>
		</div>
@endsection

@section('js')




@endsection