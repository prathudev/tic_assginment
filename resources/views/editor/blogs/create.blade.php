@extends('editor.layout')
@section('content')
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Create Blog</h3>
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-12">
					<div class="box">
						<!-- <div class="box-header with-border">
							<h4 class="box-title">Change Password</h4>
						</div> -->
						<div class="box-body">
					
							<form action="{{route('editor.blogs.store')}}" method="post" enctype="multipart/form-data">
                                
                                @csrf 
                                <div class="form-group row">
									<label class="col-form-label col-md-2">Title</label>
									<div class="col-md-10">
										<input id="title" class="form-control" type="text" name="title" required>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2">Status</label>
									<div class="col-md-10">
										<select name="status" id="status" class="form-control" required>
											<option value="1">Publish</option>
											<option value="2">Unpublish</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2">Categories</label>
									<div class="col-md-10">
										<select name="categories[]" id="categories" class="form-control select2" style="width: 100%;" multiple required>
											@foreach($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2">Image</label>
									<div class="col-md-10">
										<input id="image" class="form-control" type="file" name="image" required  accept="image/png, image/gif, image/jpeg">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2">Description</label>
									<div class="col-md-10">
										<textarea name="description" id="description" class="form-control" required></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2">Seo Details</label>
									<div class="col-md-10">
									<textarea name="seo_details" id="seo_details" class="form-control" required></textarea>
									</div>
								</div>
						
								<div class="form-group row">
									<label class="col-form-label col-md-2">Tags</label>
									<div class="col-md-10">
										<div class="tags-default">
										<!-- Lorem,Ipsum,Amet -->
											<input type="text" value="" data-role="tagsinput" placeholder="" name="tags" required/>
										</div>
									</div>
								</div>
								<div class="form-group row">
								
									<div class="col-md-10">
										<button type="submit" class="btn bg-primary">Submit</button>
										
									</div>
								</div>
							</form>
						</div>
						
					</div>
				</div>
		</div>
@endsection
@section('js')
<script>
$(function () {

	CKEDITOR.replace('description')
	CKEDITOR.replace('seo_details')

	
  });
</script>
@endsection