@extends('editor.layout')
@section('css')
	<!-- Data Table-->
	<link rel="stylesheet" type="text/css" href="/assets/vendor_components/datatable/datatables.min.css"/>
	
@endsection
@section('content')
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
				<div class="d-flex align-items-center justify-content-between">
					<div class="d-md-block d-none">
						<h3 class="page-title br-0">Blogs</h3>
                      
					</div>
                 
				</div>
                <div class="right-title">
                                  <a class="btn btn-app btn-primary" href="{{route('editor.blogs.create')}}">
									<i class="fa fa-plus"></i> Create
								  </a>
                    </div>
               
		</div>
		<div class="row">
				<div class="col-12">
					<div class="box"> 
						<div class="box-header with-border">
								<h3 class="box-title">All Blogs</h3>
						</div>
						<div class="table-responsive">						
							<table class="table table-bordered table-striped data-table">
								<thead>

									<tr>
										
										<th>ID</th>
										<th>Title</th>
										<th>Image</th>
										<th>Status</th>
										<th>Created at</th>
										<th >Action</th>

									</tr>

								</thead>

								<tbody>

								</tbody>

							</table>
						</div>
					</div>
				</div>
		</div>
@endsection

@section('js')
<!-- This is data table -->
<script src="/assets/vendor_components/datatable/datatables.min.js"></script>

<!-- assignments Admin for Data Table -->
<script src="/js/pages/data-table.js"></script>
<script type="text/javascript">

  $(function () {

    

    var table = $('.data-table').DataTable({

        processing: true,

        serverSide: true,
        order: [ [0, 'desc'] ],
        ajax: "{{route('editor.blogs.index')}}",

        columns: [

            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'image', name: 'image'},
            {data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]

    });

    

  });

</script>


@endsection